package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-create";
    }

    @Override
    @NotNull
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("Enter description: ");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final Result result = serviceLocator.getProjectEndpoint()
                .createProject(serviceLocator.getSession(), name, description);
        printResult(result);
    }

}
