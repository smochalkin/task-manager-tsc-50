package ru.smochalkin.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateAdapter extends XmlAdapter<String, Date> {

    @NotNull
    public static final String dateTransferPattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    @NotNull
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateTransferPattern);

    @Override
    @NotNull
    public String marshal(Date value) {
        return simpleDateFormat.format(value);
    }

    @Override
    @NotNull
    @SneakyThrows
    public Date unmarshal(String value) {
        return simpleDateFormat.parse(value);
    }

}

