import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.smochalkin.tm.endpoint.*;
import ru.smochalkin.tm.marker.SoapCategory;

public class AdminEndpointTest {

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private SessionDto session;

    @Before
    public void init() {
        session = sessionEndpoint.openSession("admin", "100");
        adminEndpoint.createUser(session, "guest", "10", "a@a.ru");
        adminEndpoint.removeUserByLogin(session, "test");
    }

    @Test
    @Category(SoapCategory.class)
    public void createUserTest() {
        @NotNull final Result result = adminEndpoint.createUser(session, "test", "1", "1");
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeUserByLoginTest() {
        @NotNull final Result result = adminEndpoint.removeUserByLogin(session, "guest");
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void lockUserByLoginTest() {
        @NotNull final Result result = adminEndpoint.lockUserByLogin(session, "guest");
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void unlockUserByLoginTest() {
        Assert.assertTrue(adminEndpoint.lockUserByLogin(session, "guest").isSuccess());
        Assert.assertTrue(adminEndpoint.unlockUserByLogin(session, "guest").isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllUsersTest() {
        Assert.assertNotNull(adminEndpoint.findAllUsers(session));
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllSessionsByUserIdTest() {
        @NotNull final String userId = adminEndpoint.findAllUsers(session).get(0).getId();
        Assert.assertNotNull(adminEndpoint.findAllSessionsByUserId(session,userId));
    }

    @Test
    @Category(SoapCategory.class)
    public void closeAllSessionsByUserIdTest() {
        @NotNull final String userId = adminEndpoint.findAllUsers(session).get(0).getId();
        @NotNull final Result result = adminEndpoint.closeAllSessionsByUserId(session, userId);
        Assert.assertTrue(result.isSuccess());
    }

}
