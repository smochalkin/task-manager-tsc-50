package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.dto.LogDto;

public interface ILoggerService {

    void writeLog(LogDto message);

}
