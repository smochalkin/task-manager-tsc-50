package ru.smochalkin.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.service.IReceiverService;
import ru.smochalkin.tm.listener.JmsLogListener;
import ru.smochalkin.tm.service.JmsReceiverService;

public class Bootstrap {

    @SneakyThrows
    public void init() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new JmsReceiverService(factory);
        receiverService.receive(new JmsLogListener());
    }

}
