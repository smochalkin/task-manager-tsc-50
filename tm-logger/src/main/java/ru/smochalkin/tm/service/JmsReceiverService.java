package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.service.IReceiverService;

import javax.jms.*;

public class JmsReceiverService implements IReceiverService {

    @NotNull
    private static final String JMS_LOGGER_TOPIC = "JCG_TOPIC";

    @NotNull
    private final ConnectionFactory connectionFactory;

    public JmsReceiverService(@NotNull final ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    @SneakyThrows
    public void receive(@NotNull final MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(JMS_LOGGER_TOPIC);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}
