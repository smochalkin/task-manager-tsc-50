package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.api.IBusinessService;
import ru.smochalkin.tm.dto.ProjectDto;

public interface IProjectService extends IBusinessService<ProjectDto> {
}
