package ru.smochalkin.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.endpoint.IProjectEndpoint;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.dto.result.Fail;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.result.Success;
import ru.smochalkin.tm.dto.ProjectDto;
import ru.smochalkin.tm.dto.SessionDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result createProject(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getProjectService().create(sessionDto.getUserId(), name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeProjectStatusById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "status") @NotNull final String status
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getProjectService().updateStatusById(sessionDto.getUserId(), id, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeProjectStatusByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "status") @NotNull final String status
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getProjectService().updateStatusByIndex(sessionDto.getUserId(), index, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeProjectStatusByName(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "status") @NotNull final String status
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getProjectService().updateStatusByName(sessionDto.getUserId(), name, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<ProjectDto> findProjectAllSorted(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "sort") @NotNull final String strSort
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        return serviceLocator.getProjectService().findAll(sessionDto.getUserId(), strSort);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<ProjectDto> findProjectAll(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        return serviceLocator.getProjectService().findAll(sessionDto.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectDto findProjectById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        return serviceLocator.getProjectService().findById(sessionDto.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectDto findProjectByName(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        return serviceLocator.getProjectService().findByName(sessionDto.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectDto findProjectByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        return serviceLocator.getProjectService().findByIndex(sessionDto.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeProjectById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getProjectService().removeById(sessionDto.getUserId(), id);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeProjectByName(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getProjectService().removeByName(sessionDto.getUserId(), name);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeProjectByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getProjectService().removeByIndex(sessionDto.getUserId(), index);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result clearProjects(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getProjectService().clear(sessionDto.getUserId());
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result updateProjectById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getProjectService().updateById(sessionDto.getUserId(), id, name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result updateProjectByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(sessionDto);
        try {
            serviceLocator.getProjectService().updateByIndex(sessionDto.getUserId(), index, name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}