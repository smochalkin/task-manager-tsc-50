package ru.smochalkin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.model.IWBS;
import ru.smochalkin.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    @Column
    @NotNull
    protected String name;

    @Column
    @Nullable
    protected String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Nullable
    protected Date created = new Date();

    @Nullable
    @Column(name = "start_date")
    protected Date startDate;

    @Nullable
    @Column(name = "end_date")
    protected Date endDate;

    @Nullable
    @ManyToOne
    private User user;

    @Override
    @NotNull
    public String toString() {
        return id + ": " + name + ": " + description +
                "; Status - " + status.getDisplayName() +
                "; Created - " + created +
                "; Start - " + startDate +
                "; End - " + endDate;
    }

}