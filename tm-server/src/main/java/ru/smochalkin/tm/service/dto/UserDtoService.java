package ru.smochalkin.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.dto.IUserDtoRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.*;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.exception.system.LoginExistsException;
import ru.smochalkin.tm.repository.dto.UserDtoRepository;
import ru.smochalkin.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class UserDtoService extends AbstractDtoService<UserDto> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserDtoService(
            @NotNull IPropertyService propertyService,
            @NotNull IConnectionService connectionService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isLogin(login)) throw new LoginExistsException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        @NotNull final UserDto user = new UserDto();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(getPasswordHash(password));
        user.setEmail(email);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserDtoRepository userRepository = new UserDtoRepository(entityManager);
            userRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserDto> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDto findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            return repository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    public UserDto findByLogin(@NotNull final String login) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            return repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            userRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public final void removeByLogin(@NotNull final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            userRepository.removeByLogin(login);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @Nullable final UserDto user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        @Nullable final String hash = getPasswordHash(password);
        user.setPasswordHash(hash);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @Nullable final UserDto user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
        return Optional.ofNullable(userRepository.findByLogin(login)).isPresent();
    }

    @SneakyThrows
    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final UserDto user = findByLogin(login);
        if (user == null) throw new EntityNotFoundException();
        user.setLock(true);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final UserDto user = findByLogin(login);
        if (user == null) throw new EntityNotFoundException();
        user.setLock(false);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public int getCount() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            return userRepository.getCount();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    private String getPasswordHash(@NotNull final String password) {
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        return HashUtil.salt(password, secret, iteration);
    }

}

